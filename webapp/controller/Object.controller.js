sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"../model/formatter"
], function (BaseController, JSONModel, History, formatter) {
	"use strict";

	return BaseController.extend("com.arete.zdlgkrntest.controller.Object", {

		formatter: formatter,

		
		onInit : function () {
			
			var oViewModel = new JSONModel({
					busy : true,
					delay : 0
				});

			this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);

			
			this.setModel(oViewModel, "objectView");
			
		},

		
		onNavBack : function() {
			var sPreviousHash = History.getInstance().getPreviousHash();

			if (sPreviousHash !== undefined) {
				history.go(-1);
			} else {
				this.getRouter().navTo("worklist", {}, true);
			}
		},

		
		_onObjectMatched : function (oEvent) {
			debugger;
			var sObjectId =  oEvent.getParameter("Aufnr").objectId;
			
		},


	});

});