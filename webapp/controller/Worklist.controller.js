sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (BaseController, JSONModel, formatter, Filter, FilterOperator) {
	"use strict";

	return BaseController.extend("com.arete.zdlgkrntest.controller.Worklist", {

		formatter: formatter,

		
		onInit : function () {
			var oViewModel;

			oViewModel = new JSONModel({
				
			});
			this.setModel(oViewModel, "worklistView");

			var oSmartTable = this.getView().byId("SmartTableId");     
			var oTable = oSmartTable.getTable();         
			oTable.setSelectionMode("None");
			
		},

		

		
		onPress : function (oEvent) {
			this._showObject(oEvent.getSource());
		},

		
		onNavBack : function() {
			history.go(-1);
		},


		
		_showObject : function (oItem) {
			this.getRouter().navTo("object", {
				objectId: oItem.getBindingContext().getProperty("Aufnr")
			});
		},

		
		

	});
});